- Compilar projeto usando o comando:
mvn clean install -DskipTests

- Abrir terminal e executar o comando:
docker-compose up --build --force-recreate
Obs.: Caso ocorra o erro "Caused by: java.net.ConnectException: Connection refused (Connection refused)", basta executar o comando novamente que irá funcionar.

- Link da API
http://localhost:8080/swagger-ui/index.html#/


- O JWT poderá ser validado utilizando o site
https://jwt.io/
A chave da assinatura é: secret

- Banco de dados MySQL:5.7
-- usuario: root
-- senha: "SEM SENHA"
-- jdbc:mysql://db:3306/challenge_mysql

