drop table if exists challenge_mysql.tb_acc_account;
drop table if exists challenge_mysql.tb_usr_user;
drop table if exists challenge_mysql.tb_mov_movie;

create table challenge_mysql.tb_usr_user (
    usr_cduser bigint not null auto_increment,
    usr_nmuser varchar(100) not null,
    usr_email varchar(100) not null,
    usr_password varchar(100) not null,
    usr_administrator tinyint(1) not null,
    usr_fl_exclude tinyint(1) not null,
primary key (usr_cduser),
unique index mov_name_unique (usr_email asc));

create table challenge_mysql.tb_mov_movie (
mov_cdmovie bigint not null auto_increment,
mov_name varchar(50) not null,
mov_genre varchar(45) not null,
mov_director varchar(45) not null,
mov_actor varchar(45) not null,
primary key (mov_cdmovie),
unique index mov_name_unique (mov_name asc));

create table challenge_mysql.tb_acc_account (
usr_cduser bigint(10) not null,
mov_cdmovie bigint(10) not null,
acc_count int null,
primary key (usr_cduser, mov_cdmovie),
index fk_acc_mov_idx (mov_cdmovie asc),
constraint fk_acc_usr
    foreign key (usr_cduser)
        references challenge_mysql.tb_usr_user (usr_cduser)
        on delete no action
        on update no action,
constraint fk_acc_mov
    foreign key (mov_cdmovie)
        references challenge_mysql.tb_mov_movie (mov_cdmovie)
        on delete no action
        on update no action);


-- Alimentando a base de dados
insert into challenge_mysql.tb_usr_user (usr_cduser, usr_nmuser, usr_email, usr_password, usr_administrator, usr_fl_exclude)
values ('1', 'raoni', 'raonicorreia@gmail.com', '123', 1, 0);
insert into challenge_mysql.tb_usr_user (usr_cduser, usr_nmuser, usr_email, usr_password, usr_administrator, usr_fl_exclude)
values ('2', 'rafael', 'rafael@gmail.com', '123', 0, 0);
insert into challenge_mysql.tb_usr_user (usr_cduser, usr_nmuser, usr_email, usr_password, usr_administrator, usr_fl_exclude)
values ('3', 'bruno', 'bruno@gmail.com', '123', 0, 0);

ALTER TABLE challenge_mysql.tb_usr_user AUTO_INCREMENT=4;