package br.com.ioasys.imdb.challenge.repository;

import br.com.ioasys.imdb.challenge.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findUsersByAdministratorFalseAndFlExcludeFalse(Sort sort);

    Page<User> findUsersByAdministratorFalseAndFlExcludeFalse(Pageable pageable);

    User findUsersByEmail(String email);
}
