package br.com.ioasys.imdb.challenge.controller.entity;

public class AuthorizationEntity {

    private Long id_usr;

    private boolean isAdministrator;

    public AuthorizationEntity(Long id_usr, boolean isAdministrator) {
        this.id_usr = id_usr;
        this.isAdministrator = isAdministrator;
    }

    public Long getId_usr() {
        return id_usr;
    }

    public void setId_usr(Long id_usr) {
        this.id_usr = id_usr;
    }

    public boolean isAdministrator() {
        return isAdministrator;
    }

    public void setAdministrator(boolean administrator) {
        isAdministrator = administrator;
    }
}
