package br.com.ioasys.imdb.challenge.controller;

import br.com.ioasys.imdb.challenge.controller.entity.UserEntity;
import br.com.ioasys.imdb.challenge.controller.exception.UserAlreadyExistsException;
import br.com.ioasys.imdb.challenge.controller.exception.UserNotFoundException;
import br.com.ioasys.imdb.challenge.controller.request.UserRequest;
import br.com.ioasys.imdb.challenge.model.User;
import br.com.ioasys.imdb.challenge.repository.UserRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    private final UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping(value = "/")
    @ApiOperation(value = "Listagem de usuários",
            notes = "Retorna todos os usuários cadastrado, caso não seja informado um filtro.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Lista de usuários.")
    })
    public Page<UserEntity> findUsersByAdministratorFalseAndFlExcludeFalse(Pageable pageable) {
        Page<User> pages = userRepository.
                findUsersByAdministratorFalseAndFlExcludeFalse(pageable);
        return pages.map(UserEntity::toEntity);
    }

    @PostMapping("/")
    @ApiOperation(value = "Cria novos usuários",
            notes = "Operação cria novos usuários.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Usuário criado com sucesso."),
            @ApiResponse(code = 400, message = "Usuário já existente.")
    })
    public void create(@RequestBody UserRequest user) throws UserAlreadyExistsException {
        try {
            userRepository.save(new User(user));
        } catch (DataIntegrityViolationException e) {
            throw new UserAlreadyExistsException("E-mail '"
                    + user.getEmail() +
                    "' já existe em nosso cadastro.");
        }
    }

    @PutMapping("/")
    @ApiOperation(value = "Atualiza dados do usuário",
            notes = "Operação atualiza os dados do usuário.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Usuário atualizado com sucesso."),
            @ApiResponse(code = 404, message = "Usuário não existe.")
    })
    public void update(@RequestBody UserRequest userRequest) throws UserNotFoundException {
        if (userRequest == null || userRequest.getId() == null
                || userRequest.getEmail() == null
                || userRequest.getName() == null
                || userRequest.getPassword() == null) {
            throw new UserNotFoundException("Dados do usuário deverá ser informado.");
        }
        var user = userRepository.findById(userRequest.getId());
        if (user.isPresent() && !user.get().isFlExclude()) {
            User userSave = user.get();
            userSave.setAdministrator(userRequest.isAdministrator());
            userSave.setEmail(userRequest.getEmail());
            userSave.setId(userRequest.getId());
            userSave.setName(userRequest.getName());
            userSave.setPassword(userRequest.getPassword());
            userRepository.save(userSave);
        } else {
            throw new UserNotFoundException("Usuário não localizado.");
        }
    }

    @DeleteMapping("/{id}/")
    @ApiOperation(value = "Remove um usuário",
            notes = "Operação realiza a remoção lógica de um usuário.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Usuário removido com sucesso."),
            @ApiResponse(code = 404, message = "Usuário não existe.")
    })
    public void delete(@PathVariable(required = true) Long id) throws UserNotFoundException {
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent() && !user.get().isFlExclude()) {
            user.get().setFlExclude(true);
            userRepository.save(user.get());
        } else {
            throw new UserNotFoundException("Usuário não localizado.");
        }
    }
}
