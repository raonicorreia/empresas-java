package br.com.ioasys.imdb.challenge;

import br.com.ioasys.imdb.challenge.controller.AuthenticationController;
import br.com.ioasys.imdb.challenge.controller.MovieController;
import br.com.ioasys.imdb.challenge.controller.UserController;
import br.com.ioasys.imdb.challenge.model.MovieCustom;
import br.com.ioasys.imdb.challenge.repository.MovieCustomRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(scanBasePackages = "br.com.ioasys.imdb.challenge")
@EntityScan(basePackages = "br.com.ioasys.imdb.challenge")
@EnableSwagger2
@ComponentScan(basePackageClasses = {
		AuthenticationController.class,
		MovieController.class,
		UserController.class,
		MovieCustomRepository.class,
		MovieCustom.class
})
public class ChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChallengeApplication.class, args);
	}

}
