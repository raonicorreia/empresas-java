package br.com.ioasys.imdb.challenge.controller;

import br.com.ioasys.imdb.challenge.controller.exception.LoginException;
import br.com.ioasys.imdb.challenge.controller.exception.UserNotFoundException;
import br.com.ioasys.imdb.challenge.model.security.TokenResponse;
import br.com.ioasys.imdb.challenge.model.User;
import br.com.ioasys.imdb.challenge.model.security.Constants;
import br.com.ioasys.imdb.challenge.model.security.PayLoad;
import br.com.ioasys.imdb.challenge.repository.UserRepository;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.extensions.Extension;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping(value = "/security")
public class AuthenticationController {

    private final UserRepository userRepository;

    private static final String ALGORITHM = "SHA1withRSA";

    public AuthenticationController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping("/token")
    @ApiOperation(value = "Obter um token de acesso",
            notes = "Essa operação autentica um usuário e retorna um token de acesso.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Autenticação realizada com sucesso."),
            @ApiResponse(code = 404, message = "Usuário não localizado."),
            @ApiResponse(code = 401, message = "Acesso inválido."),
            @ApiResponse(code = 403, message = "Acesso negado.")
    })
    private TokenResponse authenticate(
            @ApiParam(value = "Email do usuário", required = true, example = "email@email.com")
            @RequestParam String username,
            @ApiParam(value = "Senha", required = true, example = "123456789")
            @RequestParam String password) throws UserNotFoundException, LoginException {
        User user = userRepository.findUsersByEmail(username);
        if (user != null) {
            if (password != null && password.equals(user.getPassword())) {
                TokenResponse token = new TokenResponse();
                token.setExpires_in(Constants.EXPIRATION);
                token.setAccess_token(signToken(new PayLoad(user)));
                token.setToken_type(Constants.TOKEN_TYPE);
                return token;
            } else {
                throw new LoginException("Usuário ou senha inválidos.");
            }
        } else {
            throw new UserNotFoundException("Usuário não localizado.");
        }
    }

    private String signToken(PayLoad payLoad) {
        Algorithm algorithm = Algorithm.HMAC256("secret");
        return JWT.create()
                .withIssuer(Constants.ISSUER)
                .withSubject(payLoad.getSub())
                .withClaim("name", payLoad.getName())
                .withClaim("usr", payLoad.getUsr())
                .withIssuedAt(new Date(payLoad.getIat()))
                .withExpiresAt(new Date(payLoad.getExp()))
                .sign(algorithm);

    }
}
