package br.com.ioasys.imdb.challenge.model.security;

import br.com.ioasys.imdb.challenge.model.User;

import java.io.Serializable;

public class PayLoad {

    private String sub;

    private String name;

    // Long com a data da geracao do token
    private long iat;

    // Long com a expiracao do token
    private long exp;

    private long usr;

    public PayLoad(User user) {
        this.sub = user.getEmail();
        this.name = user.getName();
        this.iat = System.currentTimeMillis();
        // 30 minutos de validade
        this.exp = iat + Constants.EXPIRATION;
        this.usr = user.getId();
    }

    public long getUsr() {
        return usr;
    }

    public void setUsr(long usr) {
        this.usr = usr;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getIat() {
        return iat;
    }

    public void setIat(long iat) {
        this.iat = iat;
    }

    public long getExp() {
        return exp;
    }

    public void setExp(long exp) {
        this.exp = exp;
    }
}
