package br.com.ioasys.imdb.challenge.controller;

import br.com.ioasys.imdb.challenge.controller.entity.AuthorizationEntity;
import br.com.ioasys.imdb.challenge.model.MovieCustom;
import br.com.ioasys.imdb.challenge.controller.exception.AuthorizationException;
import br.com.ioasys.imdb.challenge.controller.exception.MovieAlreadyExistsException;
import br.com.ioasys.imdb.challenge.controller.exception.UserNotFoundException;
import br.com.ioasys.imdb.challenge.controller.request.MovieRequest;
import br.com.ioasys.imdb.challenge.model.*;
import br.com.ioasys.imdb.challenge.model.security.Constants;
import br.com.ioasys.imdb.challenge.repository.AccountRepository;
import br.com.ioasys.imdb.challenge.repository.MovieCustomRepository;
import br.com.ioasys.imdb.challenge.repository.MovieRepository;
import br.com.ioasys.imdb.challenge.repository.UserRepository;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/movie")
public class MovieController {

    private final MovieRepository movieRepository;

    private final MovieCustomRepository movieCustomRepository;

    private final AccountRepository accountRepository;

    private final UserRepository userRepository;

    public MovieController(MovieRepository movieRepository,
                           MovieCustomRepository movieCustomRepository,
                           AccountRepository accountRepository,
                           UserRepository userRepository) {
        this.movieRepository = movieRepository;
        this.movieCustomRepository = movieCustomRepository;
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
    }

    @GetMapping(value = "/")
    @ApiOperation(value = "Listagem dos filmes",
            notes = "Essa operação lista todos os filmes cadastrados, caso não seja informado o filtro.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Lista de filmes."),
            @ApiResponse(code = 403, message = "Acesso negado.")
    })
    public List<MovieCustom> findAll(
            @ApiParam(value = "Nome do filme", required = false, example = "email@email.com")
            @RequestParam(required = false) String name,
            @ApiParam(value = "Diretor", required = false, example = "email@email.com")
            @RequestParam(required = false) String director,
            @ApiParam(value = "Ator", required = false, example = "email@email.com")
            @RequestParam(required = false) String actor,
            @ApiParam(value = "Genero", required = false, example = "email@email.com")
            @RequestParam(required = false) String genre,
            @RequestHeader(required = false) String authorization)

            throws UserNotFoundException, AuthorizationException {
        AuthorizationEntity auth = authorize(authorization);
        if (auth != null) {
            List<MovieCustom> movies = movieCustomRepository.
                    findByFilter(name, director, actor, genre);
            return movies;
        } else {
            throw new AuthorizationException("Operação requer autenticação!");
        }
    }

    @PostMapping("/")
    @ApiOperation(value = "Inclusão de novos filmes",
            notes = "Essa operação cria novos filmes.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Filme criado com sucesso."),
            @ApiResponse(code = 400, message = "Filme já cadastrado."),
            @ApiResponse(code = 403, message = "Acesso negado.")
    })
    public void create(@RequestBody MovieRequest movieRequest,
                       @RequestHeader(required = false) String authorization)
            throws MovieAlreadyExistsException, AuthorizationException, UserNotFoundException {
        AuthorizationEntity auth = authorize(authorization);
        if (auth.isAdministrator()) {
            try {
                movieRepository.save(new Movie(movieRequest));
            } catch (DataIntegrityViolationException e) {
                throw new MovieAlreadyExistsException("Filme já cadastrado.");
            }
        } else {
            throw new AuthorizationException("Somente usuários administradores poderão realizar o cadastro.");
        }
    }

    @PostMapping("/{id_movie}/vote")
    @ApiOperation(value = "Votar em um filme",
            notes = "Essa operação contabiliza votos para um determinado filme.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Voto contabilizado com sucesso."),
            @ApiResponse(code = 403, message = "Acesso negado.")
    })
    public void vote(@PathVariable(required = true) Long id_movie,
                     @RequestHeader(required = false) String authorization)
            throws AuthorizationException, UserNotFoundException {
        AuthorizationEntity auth = authorize(authorization);
        if (!auth.isAdministrator()) {
            Optional<Accounting> account = accountRepository.findById(new AccountKey(auth.getId_usr(), id_movie));
            if (account.isPresent()) {
                Accounting acc = account.get();
                int inc = acc.getCount();
                if (acc.getCount() != null) {
                    inc += 1;
                }
                acc.setCount(inc);
                accountRepository.save(acc);
            } else {
                Accounting acc = new Accounting();
                acc.setId_movie(id_movie);
                acc.setId_user(auth.getId_usr());
                acc.setCount(1);
                accountRepository.save(acc);
            }
        } else {
            throw new AuthorizationException("Não é permitido administradores votar.");
        }
    }

    public AuthorizationEntity authorize(String authorization)
            throws AuthorizationException, UserNotFoundException {
        if (authorization == null || "".equals(authorization)) {
            throw new AuthorizationException("Operação requer autenticação! " +
                    "Um token no padrão JWT deverá ser informado.");
        }

        if (!authorization.contains("Bearer")) {
            throw new AuthorizationException("O token deverá ser do tipo Bearer.");
        }

        String[] parts = authorization.split(" ");
        if (parts.length == 2) {
            String token = parts[1];
            try {
                Algorithm algorithm = Algorithm.HMAC256("secret");
                JWTVerifier verifier = JWT.require(algorithm)
                        .withIssuer(Constants.ISSUER)
                        .build(); //Reusable verifier instance
                DecodedJWT jwt = verifier.verify(token);
                Long id = jwt.getClaims().get("usr").asLong();
                Optional<User> user = userRepository.findById(id);
                if (user.isPresent()) {
                    return new AuthorizationEntity(user.get().getId(), user.get().isAdministrator());
                } else {
                    throw new UserNotFoundException("Usuário não localizado.");
                }
            } catch (JWTVerificationException exception){
                if (exception instanceof TokenExpiredException) {
                    throw new AuthorizationException("Token expirado.");
                } else if (exception instanceof SignatureVerificationException) {
                    throw new AuthorizationException("Assinatura inválida.");
                }
                throw new AuthorizationException("Falha durante validação do token.");
            }
        }
        throw new AuthorizationException("O token inválido.");
    }
}
