package br.com.ioasys.imdb.challenge.model;

import br.com.ioasys.imdb.challenge.controller.request.UserRequest;

import javax.persistence.*;

@Entity
@Table(name = "tb_usr_user")
public class User {

    public User() {
        // Default constructor
    }

    public User(UserRequest user) {
        this.name = user.getName();
        this.email = user.getEmail();
        this.administrator = user.isAdministrator();
        this.password = user.getPassword();
        this.flExclude = false;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usr_cduser")
    private Long id;
    @Column(name = "usr_nmuser")
    private String name;
    @Column(name = "usr_email")
    private String email;
    @Column(name = "usr_password")
    private String password;
    @Column(name = "usr_administrator")
    private Boolean administrator;
    @Column(name = "usr_fl_exclude")
    private Boolean flExclude;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean isAdministrator() {
        return administrator;
    }

    public void setAdministrator(Boolean administrator) {
        this.administrator = administrator;
    }

    public Boolean isFlExclude() {
        return flExclude;
    }

    public void setFlExclude(Boolean flExclude) {
        this.flExclude = flExclude;
    }
}
