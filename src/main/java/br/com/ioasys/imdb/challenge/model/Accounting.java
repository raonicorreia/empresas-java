package br.com.ioasys.imdb.challenge.model;

import javax.persistence.*;

@Entity
@Table(name = "tb_acc_account")
@IdClass(AccountKey.class)
public class Accounting {

    @Id
    @Column(name = "usr_cduser")
    private Long id_user;
    @Id
    @Column(name = "mov_cdmovie")
    private Long id_movie;
    @Column(name = "acc_count")
    private Integer count;

    public Long getId_user() {
        return id_user;
    }

    public void setId_user(Long id_user) {
        this.id_user = id_user;
    }

    public Long getId_movie() {
        return id_movie;
    }

    public void setId_movie(Long id_movie) {
        this.id_movie = id_movie;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
