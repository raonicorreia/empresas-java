package br.com.ioasys.imdb.challenge.repository;

import br.com.ioasys.imdb.challenge.model.MovieCustom;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

@Repository
public class MovieCustomRepository {

    private final EntityManager em;

    public MovieCustomRepository(EntityManager em) {
        this.em = em;
    }

    public List<MovieCustom> findByFilter(
            String name,
            String director,
            String actor,
            String genre) {
        String query = "select M.id" +
                " , M.name" +
                " , M.genre" +
                " , M.director" +
                " , M.actor" +
                " , A.count" +
                " from Movie as M " +
                " left join Accounting as A on M.id = A.id_movie " +
                " order by A.count desc";

        String condicao = "where";
        if (name != null) {
            query += condicao + " M.name = :name";
            condicao = " and ";
        }
        if (director != null) {
            query += condicao + " M.director = :director";
            condicao = " and ";
        }
        if (actor != null) {
            query += condicao + " M.actor = :actor";
            condicao = " and ";
        }
        if (genre != null) {
            query += condicao + " M.genre = :genre";
            condicao = " and ";
        }

        var qry = em.createQuery(query, Object[].class);

        if(name != null) {
            qry.setParameter("name", name);
        }
        if(director != null) {
            qry.setParameter("director", director);
        }
        if(actor != null) {
            qry.setParameter("actor", actor);
        }
        if(genre != null) {
            qry.setParameter("genre", genre);
        }
        List<Object[]> list = qry.getResultList();
        List<MovieCustom> results = null;
        if (list != null && !list.isEmpty()) {
            results = new ArrayList<>();
            for (Object[] row : list) {
                // String name, String genre, String director, String actor
                MovieCustom container = new MovieCustom(row[1].toString(),
                        row[2].toString(), row[3].toString(),
                        row[4].toString(), row[5] != null ? ((int) row[5]) : 0);
                results.add(container);
            }
        }
        return results;
    }
}
