package br.com.ioasys.imdb.challenge.model;

import java.io.Serializable;
import java.util.Objects;

public class AccountKey implements Serializable {

    private Long id_user;

    private Long id_movie;

    public AccountKey() {
    }

    public AccountKey(Long id_user, Long id_movie) {
        this.id_user = id_user;
        this.id_movie = id_movie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountKey that = (AccountKey) o;
        return Objects.equals(id_user, that.id_user) && Objects.equals(id_movie, that.id_movie);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id_user, id_movie);
    }
}
