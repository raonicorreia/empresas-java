package br.com.ioasys.imdb.challenge.controller.entity;

import br.com.ioasys.imdb.challenge.model.Movie;

public class MovieEntity {

    private long id;

    private String name;

    private String genre;

    private String director;

    private String actor;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public static MovieEntity toEntity(Movie movie) {
        var entity = new MovieEntity();
        entity.setId(movie.getId());
        entity.setName(movie.getName());
        entity.setActor(movie.getActor());
        entity.setDirector(movie.getDirector());
        entity.setGenre(movie.getGenre());
        return entity;
    }
}
