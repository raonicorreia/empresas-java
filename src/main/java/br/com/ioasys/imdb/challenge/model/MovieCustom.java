package br.com.ioasys.imdb.challenge.model;

public class MovieCustom {

    public MovieCustom(String name, String genre, String director, String actor, int count) {
        this.name = name;
        this.genre = genre;
        this.director = director;
        this.actor = actor;
        this.count = count;
    }

    private String name;

    private String genre;

    private String director;

    private String actor;

    private int count;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
