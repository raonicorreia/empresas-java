package br.com.ioasys.imdb.challenge.model.security;

public class Constants {

    public final static long EXPIRATION = 1800000;

    public final static String TOKEN_TYPE = "Bearer";

    public final static String ISSUER = "IoasysIMDb";

}
