package br.com.ioasys.imdb.challenge.model;

import br.com.ioasys.imdb.challenge.controller.request.MovieRequest;

import javax.persistence.*;

@Entity
@Table(name = "tb_mov_movie")
public class Movie {

    public Movie() {
        // Default constructor
    }

    public Movie(MovieRequest movieRequest) {
        this.id = movieRequest.getId();
        this.name = movieRequest.getName();
        this.genre = movieRequest.getGenre();
        this.director = movieRequest.getDirector();
        this.actor = movieRequest.getActor();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "mov_cdmovie")
    private Long id;
    @Column(name = "mov_name")
    private String name;
    @Column(name = "mov_genre")
    private String genre;
    @Column(name = "mov_director")
    private String director;
    @Column(name = "mov_actor")
    private String actor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }
}
