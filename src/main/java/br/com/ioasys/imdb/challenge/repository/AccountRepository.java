package br.com.ioasys.imdb.challenge.repository;

import br.com.ioasys.imdb.challenge.model.AccountKey;
import br.com.ioasys.imdb.challenge.model.Accounting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Accounting, AccountKey> {

}
